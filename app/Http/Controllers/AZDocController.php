<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

class AZDocController extends Controller {
    protected $viewstate;
    protected $viewstategenerator;
    protected $eventvalidation;

    public function scrape($doc_number) {
    // check the doc number is valid
        if(!is_numeric($doc_number) OR strlen($doc_number) != 6) {
            return $this->badDocNumberResponse();
        }
    
    // send the initial request to get ASP.NET viewstate details
        $response = $this->initialRequest();
        $this->crawlForViewStateInfo($response->body());
    
    // submit the search form with the provided DOC number, then update the viewstate
        $response = $this->sendPost([
            '__EVENTTARGET' => 'btnNumber',
            '__VIEWSTATE' => $this->viewstate,
            '__VIEWSTATEGENERATOR' => $this->viewstategenerator,
            '__EVENTVALIDATION' => $this->eventvalidation,
            'txtNumber' => $doc_number
        ]);
        $this->crawlForViewStateInfo($response->body());

    // check that there is not a "no record found" error
        if(!$this->wasRecordFound($response->body())) {
            return $this->inmateNotFoundResponse();
        }

    // submit the link click to pull the full inmate info
        $response = $this->sendPost([
            '__EVENTTARGET' => 'gvInmate$ctl03$LinkNumber',
            '__VIEWSTATE' => $this->viewstate,
            '__VIEWSTATEGENERATOR' => $this->viewstategenerator,
            '__EVENTVALIDATION' => $this->eventvalidation,
            'txtNumber' => $doc_number
        ]);

    // crawl full inmate info for all details
        $inmate = $this->crawlFullInmateDetails($response->body());

    // return the data
        return $inmate;
    }

    protected function badDocNumberResponse() {
        return response('invalid doc number', 400);
    }

    protected function inmateNotFoundResponse() {
        return response('inmate not found', 404);
    }

    protected function wasRecordFound($body) {
        $crawler = new Crawler($body);
        return $crawler->filter('#gvInmate')->filter('tr')->filter('td')->count() == 1 ? false : true;
    }

    protected function crawlFullInmateDetails($body) {
        $crawler = new Crawler($body);
        $inmate = [];

        // ImgIMNOResult id has the photo URL
        $inmate['photoUrl'] = $crawler->filter('#ImgIMNOResult')->attr('src');

        // gridview7 table id has the name details for the inmate
        $inmate['lastName'] = $crawler->filter('#GridView7')->filter('tr')->eq(1)->filter('td')->eq(0)->text();
        $inmate['firstName'] = $crawler->filter('#GridView7')->filter('tr')->eq(1)->filter('td')->eq(1)->text();
        $inmate['middleInitial'] = $crawler->filter('#GridView7')->filter('tr')->eq(1)->filter('td')->eq(2)->text();

        // gridview8 table id has gender, height, weight, hair color
        $inmate['gender'] = $crawler->filter('#GridView8')->filter('tr')->eq(1)->filter('td')->eq(0)->text();
        $inmate['height'] = $crawler->filter('#GridView8')->filter('tr')->eq(1)->filter('td')->eq(1)->text();
        $inmate['weight'] = $crawler->filter('#GridView8')->filter('tr')->eq(1)->filter('td')->eq(2)->text();
        $inmate['hairColor'] = $crawler->filter('#GridView8')->filter('tr')->eq(1)->filter('td')->eq(3)->text();

        // gridview9 table id has eye color, ethnic origin, custody class, admission date
        $inmate['eyeColor'] = $crawler->filter('#GridView9')->filter('tr')->eq(1)->filter('td')->eq(0)->text();
        $inmate['ethnicOrigin'] = $crawler->filter('#GridView9')->filter('tr')->eq(1)->filter('td')->eq(1)->text();
        $inmate['custodyClass'] = $crawler->filter('#GridView9')->filter('tr')->eq(1)->filter('td')->eq(2)->text();
        $inmate['admissionDate'] = $crawler->filter('#GridView9')->filter('tr')->eq(1)->filter('td')->eq(3)->text();

        // gridview11 table id has projected eligible release date -> prison release date, release type
        $inmate['prisonReleaseDate'] = $crawler->filter('#GridView11')->filter('tr')->eq(2)->filter('td')->eq(0)->text();
        $inmate['releaseType'] = $crawler->filter('#GridView11')->filter('tr')->eq(2)->filter('td')->eq(1)->text();

        // gridview12 table id has most recent location info
        $inmate['complex'] = $crawler->filter('#GridView12')->filter('tr')->eq(2)->filter('td')->eq(0)->text();
        $inmate['unit'] = $crawler->filter('#GridView12')->filter('tr')->eq(2)->filter('td')->eq(1)->text();
        $inmate['lastMovement'] = $crawler->filter('#GridView12')->filter('tr')->eq(2)->filter('td')->eq(2)->text();
        $inmate['status'] = $crawler->filter('#GridView12')->filter('tr')->eq(2)->filter('td')->eq(3)->text();

        // GVCommitment table id has commitment and sentence info
        $inmate['commitments'] = [];
        $crawler->filter('#GVCommitment')->filter('tr')->reduce(function($node, $i) {
            return $i != 0;
        })->each(function($node) use (&$inmate) {
            $a = [];
            $a['commitNumber'] = $node->filter('td')->eq(0)->text();
            $a['sentenceLength'] = $node->filter('td')->eq(1)->text();
            $a['sentenceCounty'] = $node->filter('td')->eq(2)->text();
            $a['courtCauseNumber'] = $node->filter('td')->eq(3)->text();
            $a['offenseDate'] = $node->filter('td')->eq(4)->text();
            $a['sentenceDate'] = $node->filter('td')->eq(5)->text();
            $a['sentenceStatus'] = $node->filter('td')->eq(6)->text();
            $a['crime'] = $node->filter('td')->eq(7)->text();
            array_push($inmate['commitments'], $a);
        });

        // GVInfractions table id has any disciplinary infractions that may have occurred
        $inmate['disciplinaryInfractions'] = [];
        $crawler->filter('#GVInfractions')->filter('tr')->reduce(function($node, $i) {
            return $i != 0;
        })->each(function($node) use (&$inmate) {
            $a = [];
            $a['violationDate'] = $node->filter('td')->eq(0)->text();
            $a['infraction'] = $node->filter('td')->eq(1)->text();
            $a['verdictDate'] = $node->filter('td')->eq(2)->text();
            $a['verdict'] = $node->filter('td')->eq(3)->text();
            array_push($inmate['disciplinaryInfractions'], $a);
        });

        // GVProfileClass table id has profile classifications
        $inmate['profileClassifications'] = [];
        $crawler->filter('#GVProfileClass')->filter('tr')->reduce(function($node, $i) {
            return $i != 0;
        })->each(function($node) use (&$inmate) {
            $a = [];
            $a['completeDate'] = $node->filter('td')->eq(0)->text();
            $a['classificationType'] = $node->filter('td')->eq(1)->text();
            $a['custodyRisk'] = $node->filter('td')->eq(2)->text();
            $a['internalRisk'] = $node->filter('td')->eq(3)->text();
            array_push($inmate['profileClassifications'], $a);
        });

        // GVParolePlacement table id has parole placement
        $inmate['parolePlacement'] = [];
        $crawler->filter('#GVParolePlacement')->filter('tr')->reduce(function($node, $i) {
            return $i != 0;
        })->each(function($node) use (&$inmate) {
            $a = [];
            $a['custodyDate'] = $node->filter('td')->eq(0)->text();
            $a['classType'] = $node->filter('td')->eq(1)->text();
            $a['approvedDate'] = $node->filter('td')->eq(2)->text();
            $a['nextReview'] = $node->filter('td')->eq(3)->text();
            $a['paroleClass'] = $node->filter('td')->eq(4)->text();
            array_push($inmate['parolePlacement'], $a);
        });

        // GVWorkProgram table id has work programs
        $inmate['workProgram'] = [];
        $crawler->filter('#GVWorkProgram')->filter('tr')->reduce(function($node, $i) {
            return $i != 0;
        })->each(function($node) use (&$inmate) {
            $a = [];
            $a['assignedDate'] = $node->filter('td')->eq(0)->text();
            $a['completedDate'] = $node->filter('td')->eq(1)->text();
            $a['workAssignment'] = $node->filter('td')->eq(2)->text();
            array_push($inmate['workProgram'], $a);
        });

        // GVDetainer table id has Notification Requests, Detainers, and/or Warrants
        $inmate['detainers'] = [];
        $crawler->filter('#GVDetainer')->filter('tr')->reduce(function($node, $i) {
            return $i != 0;
        })->each(function($node) use (&$inmate) {
            $a = [];
            $a['detainerDate'] = $node->filter('td')->eq(0)->text();
            $a['detainerType'] = $node->filter('td')->eq(1)->text();
            $a['charges'] = $node->filter('td')->eq(2)->text();
            $a['authority'] = $node->filter('td')->eq(3)->text();
            $a['agreementDate'] = $node->filter('td')->eq(4)->text();
            $a['cancelDate'] = $node->filter('td')->eq(5)->text();
            array_push($inmate['detainers'], $a);
        });

        return $inmate;
    }

    protected function crawlForViewStateInfo($body) {
        $crawler = new Crawler($body);
        $this->viewstate = $crawler->filter('input#__VIEWSTATE')->attr('value');
        $this->viewstategenerator = $crawler->filter('input#__VIEWSTATEGENERATOR')->attr('value');
        $this->eventvalidation = $crawler->filter('input#__EVENTVALIDATION')->attr('value');

        return $this;
    }

    protected function initialRequest() {
        return Http::get(env('AZDOC_URL'));
    }

    protected function sendPost($post_data) {
        return Http::asForm()->post(env('AZDOC_URL'), $post_data);
    }
}


/* 
doc numbers:
demitri     205687
bishop      333705
rj          341139
*/
