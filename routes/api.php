<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/azdoc/scraper/inmates/{doc_number}', 'AZDocController@scrape');